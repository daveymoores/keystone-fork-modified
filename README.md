Modified version on keystone 3 to deal with this issue - https://stackoverflow.com/questions/42529265/show-more-than-50-items-on-selection-on-admin-ui


[KeystoneJS](http://keystonejs.com) is a powerful Node.js content management system and web app framework built on [express](http://expressjs.com) and [mongoose](http://mongoosejs.com). Keystone makes it easy to create sophisticated web sites and apps, and comes with a beautiful auto-generated Admin UI.

Check out [keystonejs.com](http://keystonejs.com) for documentation and guides.
